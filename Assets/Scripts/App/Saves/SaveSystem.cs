﻿using UnityAcademy.SaveSystem;
using UnityEngine;

public sealed class SaveSystem : MonoBehaviour, IDataLoadable, IDataSavable {

    [SerializeField] private SaveSystemWordsConfig _config;
    
    public void Initialize() {
        if (PlayerPrefs.HasKey(_config.PLAYER_SCORE) == false) {
            PlayerPrefs.SetInt(_config.PLAYER_SCORE, _config.DEFAULT_PLAYER_SCORE_VALUE);
        }
    }

    public int LoadHighScoreInt() {
        return PlayerPrefs.GetInt(_config.PLAYER_SCORE);
    }

    public void SaveHighScore(int value) {
        PlayerPrefs.SetInt(_config.PLAYER_SCORE, value);
    }
}
