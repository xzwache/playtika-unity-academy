﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WordsConfig", menuName = "SaveSystem/SaveSystemWordsConfig", order = 1)]
public class SaveSystemWordsConfig : ScriptableObject {
    
    public string PLAYER_SCORE = "PlayerScore";
    public int DEFAULT_PLAYER_SCORE_VALUE = 0;
}
