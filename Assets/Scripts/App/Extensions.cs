﻿using System;
using TMPro;

namespace UniRx {
    public static class UnityUIExtensions
    {
        public static IDisposable SubscribeToText(this IObservable<string> source, TextMeshProUGUI text)
        {
            return source.SubscribeWithState(text, (x, t) => t.text = x);
        }
        
        public static IDisposable SubscribeToText<T>(this IObservable<T> source, TextMeshProUGUI text)
        {
            return source.SubscribeWithState(text, (x, t) => t.text = x.ToString());
        } 
        
        public static IDisposable SubscribeToText<T>(this IObservable<T> source, TextMeshProUGUI text, Func<T, string> selector)
        {
            return source.SubscribeWithState2(text, selector, (x, t, s) => t.text = s(x));
        }
    }

    public static class LeanTweenExtensions {
        public static LTDescr LeanAlphaText (this TextMeshProUGUI textMesh, float to, float time) {
            var _color = textMesh.color;
            var _tween = LeanTween
                .value (textMesh.gameObject, _color.a, to, time)
                .setOnUpdate ((float _value) => {
                    _color.a = _value;
                    textMesh.color = _color;
                });
            return _tween;
        }
    }
}

