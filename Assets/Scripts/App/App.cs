﻿using UnityEngine;

public class App : MonoBehaviour {

    [Header("Save system")] 
    [SerializeField] private SaveSystem _saveSystem;
    
    [Header("Player camera")]
    [SerializeField] private SmoothFollow _cameraSmoothFollow;

    [Header("Player parameters")] 
    [SerializeField] private Player _player;
    [SerializeField] private MovementControll _playerMovementControll;
    [SerializeField] private PlayerCollision _collision;

    [Header("Level")] 
    [SerializeField] private Level _level;
    [Header("Statistic")]
    [SerializeField] private PlayerStats _playerStats;
    [Header("UI")] 
    [SerializeField] private MainMenuPresenter _mainMenu;
    
    void Awake() {
        // Components setup ----------
        _saveSystem.Initialize();
        _playerStats.Configure(_saveSystem);
        _mainMenu.Configure(_playerStats);
        
        // Level setup
        _level.Initialize();

        // Player score bonuses ----------
        _playerMovementControll.onSpeedUpKeyPress.AddListener(_playerStats.MaximizeScoreStep);
        _playerMovementControll.onSpeedUpKeyRelease.AddListener(_playerStats.ResetScoreStep);
        
        // Player camera move ----------
        _playerMovementControll.onSpeedUpKeyPress.AddListener(() => { MoveCamera(_cameraSmoothFollow.distance, 3.5f);});
        _playerMovementControll.onMoveForward.AddListener(() => { MoveCamera(_cameraSmoothFollow.distance, _cameraSmoothFollow.DefaultDistance); });
        
        // Player collision ----------
        _collision.onObstaclePass.AddListener(_playerStats.GiveBonus);
        _collision.onObstacleCrash.AddListener(GameOver);
        
        StartGame();

        void MoveCamera(float from, float to) {
            _cameraSmoothFollow.distance = Mathf.Lerp(from, to, 0.1f);
        }
    }

    private void StartGame() {
        _player.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
        
        _playerStats.Reset();
        _level.Reset();
        Pause();
    }
    
    public void Pause() {
        _mainMenu.ShowPauseScreen(onScreenHide:() => {
            // Enable player statistic counting and movement control
            // after pressing any key
            _playerMovementControll.LockMovement(false);
            _playerStats.LockCounting(false);
        });
        // Disable player statistic counting and movement control
        // while we not press any key
        _playerMovementControll.LockMovement(true);
        _playerStats.LockCounting(true);
    }

    private void GameOver() {
        _mainMenu.ShowGameOverScreen(StartGame);
        _playerMovementControll.LockMovement(true);
        _playerStats.LockCounting(true);
    }
}
