﻿namespace UnityAcademy {

    namespace Movement {
        public interface IRotatableInSpace {
            void RotateRight();
            void RotateLeft();
            void Stabilize();
            void LockStabilization(bool param);
        }
    }

    namespace SaveSystem {
        public interface IDataLoadable {
            int LoadHighScoreInt();
        }

        public interface IDataSavable {
            void SaveHighScore(int data);
        }
    }
}
