﻿using UnityAcademy.Movement;
using UnityEngine;

public class Spaceship : MonoBehaviour, IRotatableInSpace
{
    private Transform _transform;
    
    [SerializeField] internal float SpaceshipRotationLimit = 45.0F;
    [SerializeField] internal  float _shipRotationSpeed = 10.0f;

    private bool _stabilizationLocked = false;
    public bool StabilizationLocked => _stabilizationLocked;
    public void LockStabilization(bool param) => _stabilizationLocked = param;

    private void Start() {
        _transform = this.transform;
    }

    public void RotateRight() {
        LockStabilization(true);
        _transform.rotation = Quaternion.Slerp(_transform.rotation, Quaternion.Euler(new Vector3(0, 0, SpaceshipRotationLimit)),
            _shipRotationSpeed * Time.deltaTime);
    }
    public void RotateLeft() {
        LockStabilization(true);
        _transform.rotation = Quaternion.Slerp(_transform.rotation, Quaternion.Euler(new Vector3(0, 0, -SpaceshipRotationLimit)),
            _shipRotationSpeed * Time.deltaTime);
    }

    public void Stabilize() {
        if (_stabilizationLocked == false) {
            _transform.rotation = Quaternion.Slerp(_transform.rotation, Quaternion.Euler(Vector3.zero),
                _shipRotationSpeed / 2.0f * Time.deltaTime);
        }
    }

}