﻿using System;
using UniRx;
using UnityAcademy.SaveSystem;
using UnityEngine;

public class PlayerStats : MonoBehaviour {

    [Header("Configuration")] [SerializeField]
    private int _scoreStep;

    [SerializeField] private int _scoreStepMultiplier;
    [SerializeField] private int _scoreBonus;

    private int _currentScoreStep;
    private int _currentScore = 0;
    private int _highScore;
    private int _previousScore;
    private float _timePlayed;
    private int _obstaclesPassed;

    // Variable for control counting
    private bool _countingLocked = false;

    public ReactiveProperty<int> CurrentScore { get; private set; }
    public IReadOnlyReactiveProperty<int> HighScore { get; private set; }
    public ReactiveProperty<float> TimePlayed { get; private set; }
    public ReactiveProperty<int> ObstaclesPassed { get; private set; }
    public int PreviousScore => _previousScore;

    public void LockCounting(bool param) => _countingLocked = param;

    public void Configure<T>(T saveSystem) where T : IDataLoadable, IDataSavable {
        _previousScore = saveSystem.LoadHighScoreInt();

        // Increase score every second
        Observable.Timer(TimeSpan.FromSeconds(1.0f))
            .RepeatSafe()
            .Subscribe(_ => IncreaseScore());

        // Count time passed from start
        Observable.EveryUpdate().Subscribe(_ => IncreaseTimePlayed());

        // Configure properties ----------
        CurrentScore = new ReactiveProperty<int>(_currentScore);
        HighScore = CurrentScore.Scan(_previousScore, Math.Max).ToReactiveProperty();
        HighScore.Subscribe(_ => saveSystem.SaveHighScore(HighScore.Value));
        TimePlayed = new ReactiveProperty<float>(_timePlayed);
        ObstaclesPassed = new ReactiveProperty<int>(_obstaclesPassed);
        
        Reset();
    }

    public void GiveBonus() {
        if (_countingLocked == false) {
            CurrentScore.Value += _scoreBonus;
            IncreaseObstaclesCount();
        }
    }

    private void IncreaseScore() {
        if (_countingLocked  == false) CurrentScore.Value += _currentScoreStep;
    }

    private void IncreaseTimePlayed() {
        if (_countingLocked == false) {
            _timePlayed += Time.deltaTime;
            _timePlayed = (float) System.Math.Round(_timePlayed, 2);
            TimePlayed.Value = _timePlayed;
        }
    }

    private void IncreaseObstaclesCount() {
        if (_countingLocked == false) ObstaclesPassed.Value++;
    }

    public void MaximizeScoreStep() => _currentScoreStep = _scoreStep * _scoreStepMultiplier;
    public void ResetScoreStep() => _currentScoreStep = _scoreStep;

    public void Reset() {
        CurrentScore.Value = 0;
        ObstaclesPassed.Value = 0;
        _timePlayed = 0.0f;
        TimePlayed.Value = 0.0f;
        _currentScoreStep = _scoreStep;
    }
}
