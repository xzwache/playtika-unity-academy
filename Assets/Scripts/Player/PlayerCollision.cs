﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Events;

public class PlayerCollision : MonoBehaviour {

    [SerializeField] private string _obstacleTag = "Obstacle";
    [SerializeField] private string _obstacleCompletePassTag = "ObstacleSafeZone";

    [HideInInspector] public UnityEvent onObstacleCrash = new UnityEvent();
    [HideInInspector] public UnityEvent onObstaclePass = new UnityEvent();
    
    void Start() {
        Transform _transform = this.transform;
        _transform.OnTriggerEnterAsObservable().Where(col => col.gameObject.CompareTag(_obstacleTag)).Subscribe(_ => onObstacleCrash.Invoke());
        _transform.OnTriggerEnterAsObservable().Where(col => col.gameObject.CompareTag(_obstacleCompletePassTag)).Subscribe(_ => onObstaclePass.Invoke());
    }
}
