﻿using UnityEngine;
using UnityAcademy.Movement;

public sealed class Player : MonoBehaviour {

    [SerializeField] private MovementControll _movementControll;
    private IRotatableInSpace _vehicle; 
    
    void Start() {
        _vehicle = _movementControll.ChildVehicleBody.GetComponent<Spaceship>();
        
        _movementControll.onRotationKeysIdle.AddListener(() => _vehicle.LockStabilization(false));
        _movementControll.onMoveForward.AddListener(_vehicle.Stabilize);
        _movementControll.onLeftKeyPress.AddListener(() => {
            if (_movementControll.MovementIsLocked == false) _vehicle.RotateLeft();
        });
        _movementControll.onRightKeyPress.AddListener(() => {
            if (_movementControll.MovementIsLocked == false) _vehicle.RotateRight();
        });
    }
}
