﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class Asteroid : Obstacle {

    [Header("Body configuration parameters")]
    [SerializeField] private float _bodyRotationSpeed = 5.0f;
    [SerializeField] private float _bodyMoveThreshold = 5.0f;
    
    internal override void Configure () {
        // Rotate body
        Observable.EveryLateUpdate()
            .Subscribe(_ => _body.Rotate(Vector3.forward * _bodyRotationSpeed));
        
        // Every time when Asteroid was moved his body gets new x-position
        transform.ObserveEveryValueChanged(x => x.position)
            .Subscribe(_ => RandomizeBodyPosition());

        _safeZone.transform.OnTriggerEnterAsObservable()
            .Where(col => col.gameObject.CompareTag("Player"))
            .Subscribe(_ => onPassTrigger.Invoke());

        void RandomizeBodyPosition() {
            float x = UnityEngine.Random.Range(-_bodyMoveThreshold, _bodyMoveThreshold);
            Vector3 position = _body.transform.localPosition;
            position.x = x;
            _body.localPosition = position;
        }
    }
}
