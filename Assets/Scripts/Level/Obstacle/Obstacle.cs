﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class Obstacle : MonoBehaviour {
    
    [SerializeField] internal Transform _body;
    [SerializeField] internal Transform _safeZone;
    [HideInInspector] public UnityEvent onPassTrigger = new UnityEvent();

    internal virtual void Configure() {
        throw new NotImplementedException();
    }
}
