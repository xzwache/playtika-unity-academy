﻿using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.Events;

public class RoadPart : MonoBehaviour {
    
    [SerializeField] private Transform _respawnTrigger;

    public UnityEvent onRespawnTrigger = new UnityEvent();
    
    public void Configure() {
        _respawnTrigger.OnTriggerEnterAsObservable()
            .Where(col => col.gameObject.CompareTag("Player"))
            .Subscribe(_ => onRespawnTrigger.Invoke());
    }
}
