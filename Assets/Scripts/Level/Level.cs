﻿using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {

    [Header("Configuration")]
    [SerializeField] private float _roadPartsSpawnThreshold;
    [SerializeField] private int _roadLength;
    [SerializeField] private float _obstaclesSpawnThreshold;
    [SerializeField] private int _obstaclesCount;
    
    [Space] [Header("Difficulty parameters")] 
    [SerializeField] private float _speedIncreaseLimit;
    [SerializeField] private float _speedIncreaseStep;
    [Tooltip("In seconds")]
    [SerializeField] private float _speedIncreaseInterval;

    [Space] [Header("GameObjects data")]
    [SerializeField] private GameObject _roadPartPrefab;
    [SerializeField] private GameObject _obstaclePrefab;
    
    private RoadPart _currentRoadPart;
    private List<RoadPart> _road;

    private Obstacle _currentObstacle;
    private List<Obstacle> _obstaclesPool;
    
    public void Initialize() {
        BuildRoad();
        SpawnObstacles();
    }

    private void BuildRoad() {
        _road = new List<RoadPart>();

        for (int i = 0; i < _roadLength; i++) {
            _currentRoadPart = Instantiate(_roadPartPrefab, this.transform).GetComponent<RoadPart>();
            _road.Add(_currentRoadPart);
            
            _currentRoadPart.transform.localPosition = new Vector3(0.0f, 0.0f, _roadPartsSpawnThreshold * i);
            
            _currentRoadPart.onRespawnTrigger.AddListener(SetPartToEnd);
            _currentRoadPart.Configure();
        }
    }

    private void SpawnObstacles() {
        _obstaclesPool = new List<Obstacle>();
        
        for (int i = 0; i < _obstaclesCount; i++) {
            _currentObstacle = Instantiate(_obstaclePrefab, this.transform).GetComponent<Obstacle>();
            _obstaclesPool.Add(_currentObstacle);

            _currentObstacle.transform.localPosition = new Vector3(0.0f, 2.0f, _obstaclesSpawnThreshold * i);
            
            _currentObstacle.gameObject.SetActive(false);
            _currentObstacle.gameObject.SetActive(true);
            
            _currentObstacle.onPassTrigger.AddListener(SetObstacleToEnd);
            _currentObstacle.Configure();
        }
    }

    private void SetPartToEnd() {
        _currentRoadPart = _road[0];
        _road.Remove(_currentRoadPart);
        Vector3 position = _currentRoadPart.transform.localPosition;
        float newPositionZ = position.z + (_roadLength * _roadPartsSpawnThreshold);
        position.z = newPositionZ;
        _currentRoadPart.transform.localPosition = position;

        _road.Add(_currentRoadPart);
    }

    private void SetObstacleToEnd() {
        _currentObstacle = _obstaclesPool[0];
        _obstaclesPool.Remove(_currentObstacle);
        Vector3 position = _currentObstacle.transform.localPosition;
        float newPositionZ = position.z + (_obstaclesCount * _obstaclesSpawnThreshold);
        position.z = newPositionZ;
        _currentObstacle.transform.localPosition = position;
        
        _obstaclesPool.Add(_currentObstacle);
    }

    public void Reset() {
        RearrangeRoad();
        RearrangeObstacles();
    }

    private void RearrangeRoad() {
        for (int i = 0; i < _road.Count; i++) {
            Vector3 position = Vector3.zero;
            position.z = _roadPartsSpawnThreshold * i;
            _currentRoadPart = _road[i];
            _currentRoadPart.transform.localPosition = position;
        }
    }

    private void RearrangeObstacles() {
        for (int i = 0; i < _obstaclesPool.Count; i++) {
            Vector3 position = Vector3.zero;
            position.z = _obstaclesSpawnThreshold * i;
            position.y = 2.0f;
            _currentObstacle = _obstaclesPool[i];
            _currentObstacle.transform.localPosition = position;
        }
    }
    
}
