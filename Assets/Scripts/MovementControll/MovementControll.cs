﻿using UnityEngine;
using UnityEngine.Events;

public class MovementControll : MonoBehaviour {
    
    [Header("Buttons")]
    [SerializeField] internal KeyCode _moveLeftButton1 = KeyCode.A;
    [SerializeField] internal KeyCode _moveLeftButton2 = KeyCode.LeftArrow;
    
    [SerializeField] internal KeyCode _moveRightButton1 = KeyCode.D;
    [SerializeField] internal KeyCode _moveRightButton2 = KeyCode.RightArrow;

    [SerializeField] internal KeyCode _speedUpButton = KeyCode.Space;

    [Space] [Header("Movement configuration")] 
    [SerializeField] internal float _forwardMovementSpeed = 1.0f;
    [SerializeField] internal float _dodgeSpeed = 0.2f;

    [SerializeField] internal int BoostSpeedMultiplier = 2;
    [SerializeField] internal float _accelerationDuration;

    internal float _currentMovementSpeed;
    internal float _boostSpeed;
    internal float _startMovementTime;

    internal bool _movementIsLocked;

    [Space] [Header("Vehicle")]
    [SerializeField] internal Transform _childVehicleBody;

    [Space] [Header("Movement events")]
    public UnityEvent onMoveForward = new UnityEvent();
    public UnityEvent onLeftKeyPress = new UnityEvent();
    public UnityEvent onRightKeyPress = new UnityEvent();
    public UnityEvent onSpeedUpKeyPress = new UnityEvent();
    public UnityEvent onSpeedUpKeyRelease = new UnityEvent();
    public UnityEvent onRotationKeysIdle = new UnityEvent();
    
    internal Transform _transform;
    internal Rigidbody _rigidbody;

    public bool MovementIsLocked => _movementIsLocked;

    public void LockMovement(bool param) {
        _movementIsLocked = param;
        
        // Reset start movement time when we again start moving
        if (param == false) _startMovementTime = Time.time;
    }

    public Transform ChildVehicleBody => _childVehicleBody;

    internal virtual void MoveForward() {
        throw new System.NotImplementedException();
    }

    internal virtual void MoveLeft() {
        throw new System.NotImplementedException();
    }

    internal virtual void MoveRight() {
        throw new System.NotImplementedException();
    }

    internal virtual void SpeedUp() {
        throw new System.NotImplementedException();
    }

    internal virtual void SlowDown() {
        throw new System.NotImplementedException();
    }
}
