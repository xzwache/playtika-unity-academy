﻿using UniRx;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public sealed class PlayerMovementControll : MovementControll {

    void Start() {
        _rigidbody = GetComponent<Rigidbody>();
        _transform = this.gameObject.transform;

        _startMovementTime = Time.time;

        _currentMovementSpeed = _forwardMovementSpeed;
        _boostSpeed = _forwardMovementSpeed * BoostSpeedMultiplier;
        
        onMoveForward.AddListener(MoveForward);
        onLeftKeyPress.AddListener(MoveLeft);
        onRightKeyPress.AddListener(MoveRight);
        onRotationKeysIdle.AddListener(() => _rigidbody.velocity = Vector3.zero);
        
        onSpeedUpKeyPress.AddListener(SpeedUp);

        var rightButtonKeyPressStream = Observable.EveryUpdate().Where(_ => Input.GetKey(_moveLeftButton1) || Input.GetKey(_moveLeftButton2));
        var leftButtonKeyPressStream = Observable.EveryUpdate().Where(_ => Input.GetKey(_moveRightButton1) || Input.GetKey(_moveRightButton2));

        // Configure forward movement
        Observable.EveryUpdate().
            Subscribe(x => {
                onMoveForward.Invoke();
            });
        
        // Configure speed-up on press "Speed-up" button
        Observable.EveryUpdate()
            .Where(_ => Input.GetKey(_speedUpButton))
            .Subscribe(_ => {
                onSpeedUpKeyPress.Invoke();
            });
        
        // Configure slow down on release "Speed-up" button
        Observable.EveryUpdate()
            .Where(_ => Input.GetKeyUp(_speedUpButton))
            .Subscribe(_ => {
                SlowDown();
                onSpeedUpKeyRelease.Invoke();
            });

        // On control keys "idle"
        Observable.EveryUpdate()
            .Where(_ => !Input.GetKey(_moveLeftButton1) || !Input.GetKey(_moveLeftButton2) ||
                        !Input.GetKey(_moveRightButton1) || !Input.GetKey(_moveRightButton2))
            .Subscribe(_ => onRotationKeysIdle.Invoke());

        leftButtonKeyPressStream.Subscribe(num => onLeftKeyPress.Invoke());
        rightButtonKeyPressStream.Subscribe(num => onRightKeyPress.Invoke());
    }
    
    internal override void MoveForward() {
        if (_movementIsLocked == false) {
            float t = (Time.time - _startMovementTime) / _accelerationDuration;
            float speed = Mathf.SmoothStep(0.0f, _currentMovementSpeed, t);
            transform.Translate(Vector3.forward * speed);
        }
    }
    internal override void MoveLeft() { if (_movementIsLocked == false) _rigidbody.velocity = Vector3.right * _dodgeSpeed;}
    internal override void MoveRight() { if (_movementIsLocked == false) _rigidbody.velocity = Vector3.left * _dodgeSpeed;}
    internal override void SpeedUp() { if (_movementIsLocked == false) _currentMovementSpeed = _boostSpeed; }
    internal override void SlowDown() { if (_movementIsLocked == false) _currentMovementSpeed = _forwardMovementSpeed;}
}
