﻿using UnityEngine;

public class PlanetFollow : MonoBehaviour {
    
    [SerializeField] private Transform target;
    [SerializeField] private float _zOffset;
    
    private void Update() {
        var position = transform.position;
        position.z = target.position.z + _zOffset;
        position.x = target.position.x + 130f;
        transform.position = position;
    }
}
