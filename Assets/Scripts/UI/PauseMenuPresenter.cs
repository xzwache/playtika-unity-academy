﻿using System;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class PauseMenuPresenter : UIPresenter {

    [Header("UI elements")]
    [SerializeField] private TextMeshProUGUI _pressAnyKeyText;
    
    public void Configure(Action onAnyKeyPressed) {
        
        ConfigureUI();
        ConfigureUIAnimations();
        BindEventsToAnyKeyPressed(onAnyKeyPressed);
    }

    private void ConfigureUI() {
        gameObject.OnEnableAsObservable()
            .Subscribe(_ => {
                _pressAnyKeyText.alpha = 1.0f;
            });
    }
    

    private void BindEventsToAnyKeyPressed(Action onAnyKeyPressed) {
        Observable.EveryUpdate()
            .Where(_ => Input.anyKey && gameObject.activeInHierarchy)
            .Subscribe(_ => {
                // if screen is showed then onAnyKeyPressed can be invoked
                if (gameObject.activeInHierarchy) onAnyKeyPressed.Invoke();
            });
    }

    internal override void ConfigureUIAnimations() {
        gameObject.OnEnableAsObservable()
            .Subscribe(_ => {
                LeanTween.alpha(_background.rectTransform, 0.5f, 1.2f);
                _pressAnyKeyText.LeanAlphaText(0.0f, 0.8f)
                    .setLoopPingPong().setDelay(0.5f);
            });
    }

    public override void Show() {
        this.gameObject.SetActive(true);
    }
    
    public override void Hide() {
        _pressAnyKeyText.LeanAlphaText(0.0f, 0.8f);
        LeanTween.alpha(_background.rectTransform, 0.0f, 0.8f)
            .setOnComplete(() => {
                this.gameObject.SetActive(false);
                
                onScreenHide.Invoke();
            });
    }
}
