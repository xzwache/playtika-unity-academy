﻿using System;
using UniRx;
using UniRx.Triggers;
using TMPro;
using UnityEngine;

public class GameOverMenuPresenter : UIPresenter {
    
    [SerializeField] private TextMeshProUGUI _gameOverText;
    [SerializeField] private TextMeshProUGUI _currentScoreText;
    [SerializeField] private TextMeshProUGUI _timePlayedText;
    [SerializeField] private TextMeshProUGUI _obstaclesPassedText;
    [SerializeField] private TextMeshProUGUI _congratulationText;

    [SerializeField] private UnityEngine.UI.Button _restartButton;
    
    [Header("UI Text values")]
    [SerializeField] private string _scoreTextValue;
    [SerializeField] private string _obstaclesPassedTextValue;
    [SerializeField] private string _timePlayedTextValue;

    public void Configure(PlayerStats stats) {
        stats.CurrentScore.SubscribeToText(_currentScoreText, value => {
            if (stats.CurrentScore.Value > stats.PreviousScore) {
                _congratulationText.gameObject.SetActive(true);
            } else _congratulationText.gameObject.SetActive(false);
            return string.Format($"{_scoreTextValue.ToUpper()}: {value}");
        });
        stats.TimePlayed.SubscribeToText(_timePlayedText, value => string.Format($"{_timePlayedTextValue.ToUpper()}: {value}"));
        stats.ObstaclesPassed.SubscribeToText(_obstaclesPassedText, value => string.Format($"{_obstaclesPassedTextValue.ToUpper()}: {value}"));

        ConfigureUIAnimations();
    }

    internal override void ConfigureUIAnimations() {
        Color backgroundAnimationColor = Color.black;
        backgroundAnimationColor.a = 0.5f;
        
        ResetTextsAlpha();
        
        // Show UI smoothly on enable
        gameObject.OnEnableAsObservable()
            .Subscribe(_ => {
                // Show background
                LeanTween.alpha(_background.rectTransform, 1.0f, 1.0f)
                    .setOnComplete(() => {
                        // Animate background with color
                        LeanTween.color(_background.rectTransform, backgroundAnimationColor, 2.0f).setLoopPingPong();
                    });
                ShowUIParts();
            });


        void ShowUIParts() {
            _gameOverText.LeanAlphaText(1.0f, 0.8f).setDelay(0.5f);
            _congratulationText.LeanAlphaText(1.0f, 0.8f).setDelay(0.5f);
            _currentScoreText.LeanAlphaText(1.0f, 0.8f).setDelay(0.5f);
            _obstaclesPassedText.LeanAlphaText(1.0f, 0.8f).setDelay(0.5f);
            _timePlayedText.LeanAlphaText(1.0f, 0.8f).setDelay(0.5f);
            LeanTween.alpha(_restartButton.GetComponent<RectTransform>(), 1.0f, 1.4f).setDelay(1.0f);
            _restartButton.GetComponentInChildren<TextMeshProUGUI>().LeanAlphaText(1.0f, 1.4f).setDelay(1.0f);
        }

        void ResetTextsAlpha() {
            _gameOverText.LeanAlphaText(0.0f, 0.0f);
            _congratulationText.LeanAlphaText(0.0f, 0.0f);
            _currentScoreText.LeanAlphaText(0.0f, 0.0f);
            _obstaclesPassedText.LeanAlphaText(0.0f, 0.0f);
            _timePlayedText.LeanAlphaText(0.0f, 0.0f);
        }
    }

    public void Show(Action onRestartButtonClick) {
        this.gameObject.SetActive(true);
        
        _restartButton.onClick.RemoveAllListeners();
        _restartButton.onClick.AddListener(onRestartButtonClick.Invoke);
        
    }
}
