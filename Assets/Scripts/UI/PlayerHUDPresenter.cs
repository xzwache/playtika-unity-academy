﻿using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class PlayerHUDPresenter : UIPresenter {
    
    [SerializeField] private TextMeshProUGUI _currentScoreText;
    [SerializeField] private TextMeshProUGUI _highScoreText;
    [SerializeField] private TextMeshProUGUI _timePlayedText;
    [SerializeField] private TextMeshProUGUI _obstaclesTextDescription;
    [SerializeField] private TextMeshProUGUI _obstaclesPassedText;
    
    [Header("UI Text values")] [SerializeField]
    private string _bestScoreTextValue;

    public void Configure(PlayerStats stats) {
        BindStatisticToUI(stats);
        ConfigureUIAnimations(stats);
    }

    private void BindStatisticToUI(PlayerStats stats) {
        stats.CurrentScore.SubscribeToText(_currentScoreText);
        stats.HighScore.SubscribeToText(_highScoreText, value => string.Format($"{_bestScoreTextValue.ToUpper()}: {value}"));
        stats.TimePlayed.SubscribeToText(_timePlayedText);
        stats.ObstaclesPassed.SubscribeToText(_obstaclesPassedText);
    }

    private void ConfigureUIAnimations(PlayerStats stats) {
        stats.CurrentScore.Subscribe(_ => Animate(_currentScoreText.gameObject));
        stats.HighScore.Subscribe(_ => Animate(_highScoreText.gameObject));
        stats.ObstaclesPassed.Subscribe(_ => Animate(_obstaclesPassedText.gameObject));
        
        HideText();
        
        gameObject.OnEnableAsObservable()
            .Subscribe(_ => {
                LeanTween.alpha(_background.rectTransform, 0, 0.8f);
                _currentScoreText.LeanAlphaText(1, 0.8f).setDelay(0.5f);
                _highScoreText.LeanAlphaText(1, 0.8f).setDelay(0.5f);
                _obstaclesPassedText.LeanAlphaText(1, 0.8f).setDelay(0.5f);
                _obstaclesTextDescription.LeanAlphaText(1, 0.8f).setDelay(0.5f);
                _timePlayedText.LeanAlphaText(1, 0.8f).setDelay(0.5f);
            });
        
        gameObject.OnDisableAsObservable()
            .Subscribe(_ => {
                _currentScoreText.LeanAlphaText(0, 0.4f);
                _highScoreText.LeanAlphaText(0, 0.4f);
                _obstaclesPassedText.LeanAlphaText(0, 0.4f);
                _obstaclesTextDescription.LeanAlphaText(0, 0.4f);
                _timePlayedText.LeanAlphaText(0, 0.4f);
            });

        void Animate(GameObject target) {
            LeanTween.scale(target, Vector3.one, 0.2f)
                .setFrom(Vector3.one * 0.5f)
                .setEase(LeanTweenType.easeOutBack);
        }
        
        void HideText() {
            _currentScoreText.LeanAlphaText(0.0f, 0.0f);
            _highScoreText.LeanAlphaText(0.0f, 0.0f);
            _obstaclesPassedText.LeanAlphaText(0.0f, 0.0f);
            _obstaclesTextDescription.LeanAlphaText(0.0f, 0.0f);
            _timePlayedText.LeanAlphaText(0.0f, 0.0f);
        }
    }
}
