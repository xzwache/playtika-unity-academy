﻿using System;
using UnityEngine;

public class MainMenuPresenter : MonoBehaviour {

    [SerializeField] private PauseMenuPresenter _pauseMenu;
    [SerializeField] private PlayerHUDPresenter _playerHud;
    [SerializeField] private GameOverMenuPresenter _gameOverScreen;
    
    public void Configure(PlayerStats stats) {
        _pauseMenu.Configure(() => {
            _playerHud.Show();
            _pauseMenu.Hide();
        });
        
        _playerHud.Configure(stats);
        _gameOverScreen.Configure(stats);
        
        _playerHud.gameObject.SetActive(false);
        _playerHud.gameObject.SetActive(false);
        _gameOverScreen.gameObject.SetActive(false);
    }

    public void ShowPauseScreen(Action onScreenHide) {
        _playerHud.Hide();
        _gameOverScreen.Hide();
        
        _pauseMenu.Show();
        _pauseMenu.onScreenHide.RemoveAllListeners();
        _pauseMenu.onScreenHide.AddListener(onScreenHide.Invoke);
    }

    public void ShowGameOverScreen(Action onRestartButtonClick) {
        _playerHud.Hide();
        _gameOverScreen.Show(onRestartButtonClick);
    }
}
