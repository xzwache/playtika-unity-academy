﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIPresenter : MonoBehaviour
{
    [Header("UI elements")]
    [SerializeField] internal Image _background;
    
    [HideInInspector] public UnityEvent onScreenHide = new UnityEvent();
    
    public virtual void Configure() {
        throw new NotImplementedException();
    }
    
    internal virtual void ConfigureUIAnimations() {
        throw new NotImplementedException();
    }

    public virtual void Show() {
        this.gameObject.SetActive(true);
    }

    public virtual void Hide() {
        this.gameObject.SetActive(false);
    }
}
